#! /usr/bin/python3
import requests
from bs4 import BeautifulSoup

def search(keywords):
    split_keywords = str(keywords.replace(' ', '+'))
    results_page = requests.get('http://duckduckgo.com/html/?q={}'.format(split_keywords))
    parsed_results_page = BeautifulSoup(results_page.content, 'html.parser')

    result_titles = parsed_results_page.find_all('a', class_ = 'result__a')
    results_content = parsed_results_page.find_all(class_ = 'result__snippet')

    for result_title in result_titles[1:5]:
        result_titles[result_titles.index(result_title)] = result_title.get_text()
    for result_content in results_content[1:5]:
        results_content[results_content.index(result_content)] = result_content.get_text()

    return (result_titles[1:5], results_content[1:5])
