#! /usr/bin/python3
import speech_recognition

def listen_and_process():
    recognizer = speech_recognition.Recognizer()
    mic = speech_recognition.Microphone()
    with mic as source:
        recognizer.adjust_for_ambient_noise(source)
        command = recognizer.listen(source)
        interpreted_command = recognizer.recognize_google(command)
        return interpreted_command
